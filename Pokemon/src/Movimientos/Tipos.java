package Movimientos;

/**
 *Nombramos a los diferentes tipos de poke y de moviemientos
 * @author Ismael
 *
 */
public enum Tipos {

	NINGUNO,
	NORMAL,
	PLANTA,
	FUEGO,
	AGUA,
	ELECTRICO,
	ROCA,
	ACERO,
	SINIESTRO,
	DRAGON,
	FANTASMA,
	PSIQUICO,
	HADA,
	HIELO,
	VOLADOR,
	LUCHA,
	VENENO,
	TIERRA,
	BICHO,
	;
}
