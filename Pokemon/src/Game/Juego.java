 package Game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import Core.Board;
import Core.BoardSprite;
import Core.Boton;
import Core.Field;
import Core.Sprite;
import Core.Texto;
import Core.Window;
import Mounstruitos.Ataque;
import Mounstruitos.ListaPokemon;
import Mounstruitos.Pokemon;
import Mounstruitos.PokemonName;

/**
 * Esta clase es el propio juego, contiene el main y esas cosas
 * A la hora de los combates, hay que darle debajo del recuadro azul 
 * con el raton para atacar, porque se movio y no sabia como ponerlo bien.
 * 
 **
 * Movimiento
 * Controles------- a,s,d,w      -----  p para ir mas rapido -------  q para obtener la pokeball 
 *  * ------- e para ver el pokemon del inventario
 * @author Ismael
 */
public class Juego {
	
	//{{  Medidas 
	static int size = 0;
	static int sizeMap = 0;
	//}}
	 
	//{{ Todo
	public static Random r = new Random();
	public static Field f = new Field();
	public static Window w = new Window(f);
	static Personaje pj = Personaje.getPersonaje();
	static Entrenador et1;
	static EntrenadorC et2;
	static EntrenadorE et3;
    static Mapa mp = new Mapa("Ciudad", 0+sizeMap, 0+sizeMap, 1024+sizeMap, 1024+sizeMap, "res/Pueblo1.png");
    static ColPared casaAmarilla = new ColPared("casa amarilla", 161+sizeMap, 84+sizeMap, 246+sizeMap, 191+sizeMap, "");
    static Hierba hierba = new Hierba("hierba", 134, 200, 160, 229, "");
    private static Hierba ultimaHierba = null;
    static Pokeluca pokeluca;
    public static Board b = new Board();
    //}}

    //{{  Ultima posicion raton
	private static int[] lastMousePosition = {-1,-1};
	//}}
	
	//{{  Equipo
	public static int posicionequipo;
	//}}
	
	//{{  Finalizar duelo
	public static boolean acabarCombate = false;
	//}}
	
	//{{  Lista de sprites
	private static ArrayList<Sprite> sprites = new ArrayList<>();
	//}}
 
	/**
	 * Inicializa todo, pilla inputs y dibuja la pantalla
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		init();
		
		while(true) {	
			pj.pjInput();
			
			chekcsAndCollisions();

			f.background=null;
			f.draw(sprites);
			Thread.sleep(33);
		}
	}

	/**
	 * Te dice las colisiones que hace tu pj
	 */
	private static void chekcsAndCollisions() throws InterruptedException {

		checkForPokeballs();
		checkForEntrenadores();
		//Resetear la ultima hierba pisada
		if(ultimaHierba != null && !pj.collidesWith(ultimaHierba)) {
			ultimaHierba = null;
		}
		
		//Al chocar con hierba hacer encuentro
		for (Sprite s : sprites) {
			if(s instanceof Hierba) {
				Hierba h = (Hierba) s;
				if(pj.collidesWith(h) && ultimaHierba != h) { 
					//En vez de hacerse cada vez que colisiona con la hierva, hacer que sea con cada nueva hierva
					System.out.println("AAAAAAAAAAAAAAAAAAA, NO ME GUSTA LA HIERBA");
					int prob = 50;
					int randNum = r.nextInt(100)+1;
					System.out.println("Deber�a entrar en combate? "+prob +" "+randNum);
					if(randNum<=prob) {
						encuentro();
						ultimaHierba = h;
						Thread.sleep(33);
					}
				}
			}
		}
		
		//Colisiones con paredes
		for (int i = 0; i < sprites.size(); i++) {
			if(pj.collidesWith(sprites.get(i))) { //AQUI MIRA SI TU PERSONAJE COLISIONA CON OTRO SPRITE
				if(sprites.get(i).terrain == true) { //MIRA SI EL SPRITE QUE HA COLISIONADO TU PERSONAJE ES DE TIPO TERRENO
					if(pj.headOn(sprites.get(i))) {
						pj.y1+= pj.speed;
						pj.y2+= pj.speed;
						f.scroll(0, -pj.speed);
					}
					if(pj.leftOn(sprites.get(i))) {
						pj.x1+= pj.speed;
						pj.x2+= pj.speed;
						f.scroll(-pj.speed, 0);
					}
					if(pj.rightOn(sprites.get(i))) {
						pj.x1-= pj.speed;
						pj.x2-= pj.speed;
						f.scroll(pj.speed, 0);
					}
					if(pj.stepsOn(sprites.get(i))) {
						pj.y1-= pj.speed;
						pj.y2-= pj.speed;
						f.scroll(0,pj.speed);
					}
				}
			}
		}		
	}


	/**
	 * Encuentro con pokemon salvaje, de momento solo esta uno
	 */
	public static void encuentro() throws InterruptedException {
		ListaPokemon.otherPokemons(); // Instancia los pokemon que puede aparecer
		Random numAleatorio = new Random();
		int option = numAleatorio.nextInt(8)+1;
		Pokemon enemigo = null;
		switch (option) {
			case 1:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Magikarp);
				break;
			case 2:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Dragonite);
				break;
			case 3:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Metapod);
				break;
			case 4:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Zeraora);
				break;
			case 5:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Magnezone);
				break;
			case 6:
				enemigo = ListaPokemon.pokemons.get(PokemonName.PikachuAsh);
				break;
			case 7:
				enemigo = ListaPokemon.pokemons.get(PokemonName.RubenWopper);
				break;
			case 8:
				enemigo = ListaPokemon.pokemons.get(PokemonName.Ferrothorn);
				break;
		}
		
			

		System.out.println("encuentro " + enemigo);
		
		Pokemon nuestro = null;
		
		boolean canCombat = true;
		
		if(pj.actualPokemons > 0) {
			for (int i = 0; i < pj.actualPokemons; i++) {
				if(pj.pokemonInventory[i].hp > 0) {
					nuestro = pj.pokemonInventory[i];
					break;
				}
			}

			Icombate(nuestro, enemigo, false);
		
		}
		
	}

	/**
	 * Inicializa el combate entre nuestro pokemon y el salvaje
	 */
	private static boolean Icombate(Pokemon nuestro, Pokemon enemigo, boolean isEntrenador) throws InterruptedException {
		boolean nuestroTurno;
		f.resetScroll();
		f.clear();
		f.resize();
//		w.changeSize(550, 400);
		
		System.out.println(w.getWidth());
		System.out.println(nuestro + "" + enemigo);
		if (nuestro.spd >= enemigo.spd) {
			nuestroTurno = true;
		} else {
			nuestroTurno = false;
		}
		
		ArrayList<Sprite> toDraw = new ArrayList<Sprite>();
		toDraw.add(new Sprite("BG", 0, 0, 640, 450, "res/Pruebafinal.png", false));
		
		toDraw.add(new Sprite("Enemy", (w.getWidth()/5)*4-64-20, (w.getHeight()/5)*2-64, (w.getWidth()/5)*4+64-20,
				(w.getHeight()/5)*2+64, enemigo.path, false));
		toDraw.add(new Sprite("Nuestro", (w.getWidth()/5)*1-48, (w.getHeight()/5)*3-48, (w.getWidth()/5)*1+48,
				(w.getHeight()/5)*3+48, nuestro.path, false));
				
		Texto txtNuestroHp = new Texto("txtNuestroHp", (w.getWidth()/5)*3, (w.getHeight()/5)*3, ("PS " + nuestro.name + " = " + nuestro.hp));
		txtNuestroHp.font = Texto.lightFont;
		txtNuestroHp.textColor = Texto.whiteColor;
		toDraw.add(txtNuestroHp);
		
		Texto txtEnemigoHp = new Texto("txtNuestroHp", (w.getWidth()/5)*1-((w.getWidth()/5)*1)/2, (w.getHeight()/5)*1-((w.getHeight()/5)*1)/2, ("PS " + enemigo.name + " = " + enemigo.hp));
		txtEnemigoHp.font = Texto.lightFont;
		txtEnemigoHp.textColor = Texto.whiteColor;
		toDraw.add(txtEnemigoHp);
		
		// 1=Lucha, 2=Mochila, 3=Pokemon, 4=Huir
		toDraw.add(new Boton("1", (w.getWidth()/2)*1+(w.getWidth()/24), (w.getHeight()/32)*24, (w.getWidth()/16)*13, (w.getHeight()/32)*28));
		toDraw.add(new Boton("2", (w.getWidth()/16)*13, (w.getHeight()/32)*24, w.getWidth(), (w.getHeight()/32)*28));
		toDraw.add(new Boton("3", (w.getWidth()/2)*1+(w.getWidth()/24), (w.getHeight()/32)*28, (w.getWidth()/16)*13, w.getHeight()));
		toDraw.add(new Boton("4", (w.getWidth()/16)*13, (w.getHeight()/32)*28, w.getWidth(), w.getHeight()));
		acabarCombate = false;
		boolean huirDeCombate = false, combateGanado = false;
		f.draw(toDraw);
		Thread.sleep(1000);
		while (!acabarCombate) {
			if(nuestroTurno) {
				Thread.sleep(1000);
				int toChoose = -1;
				int toChooseAtaque = -1;
				while (toChoose == -1) {
					toChoose = checkMouseInt(toDraw);
					f.draw(toDraw);
					Thread.sleep(33);
				}
				System.out.println(toChoose);
				switch (toChoose) {
					case 1:  //ATACAR
						ArrayList<Sprite> toDrawAtacks = new ArrayList<Sprite>();
						for (int i = 0; i < nuestro.ataques.length; i++) {
							if(nuestro.ataques[i] == null)
								break;
							int x1=0,x2=0,y1=0,y2=0;
							switch (i) {
							case 0:
								x1 = (w.getWidth()/24);
								y1 = (w.getHeight()/32)*24;
								x2 = (w.getWidth()/16)*5;
								y2 = (w.getHeight()/32)*28;
								break;
							case 1:
								x1 = (w.getWidth()/16)*5;
								y1 = (w.getHeight()/32)*24;
								x2 = w.getWidth()/2;
								y2 = (w.getHeight()/32)*28;
								break;
							case 2:
								x1 = (w.getWidth()/24);
								y1 = (w.getHeight()/32)*28;
								x2 = (w.getWidth()/16)*5;
								y2 = w.getHeight();
								break;
							case 3:
								x1 = (w.getWidth()/16)*5;
								y1 = (w.getHeight()/32)*28;
								x2 = w.getWidth()/2;
								y2 = w.getHeight();
								break;
							default:
								continue;

							}
							System.out.println(x1+" "+y1+" "+x2+" "+y2);
							toDrawAtacks.add(new Boton(i+"", x1,y1,x2,y2, "res/button.png"));
							System.out.println(nuestro.ataques[i].name + " \\ " + i+"");
							Texto txtAtaque = new Texto(nuestro.ataques[i].name, x1+((x2-x1)/4), y1+((y2-y1)/2), nuestro.ataques[i].name);
							txtEnemigoHp.font = Texto.lightFont;
							txtEnemigoHp.textColor = Texto.whiteColor;
							toDrawAtacks.add(txtAtaque);
						}
						toDraw.addAll(toDrawAtacks);
						while (toChooseAtaque == -1) {
							toChooseAtaque = checkMouseInt(toDraw);
							f.draw(toDraw);
							Thread.sleep(33);
						}
						for (Sprite s : toDrawAtacks) {
							for (int i = 0; i < toDraw.size(); i++) {
								if(s==toDraw.get(i)) {
									toDraw.remove(s);
									i--;
								}
							}
						}
						if(toChooseAtaque>=4) //Cancelar ataque
							continue;
						int damages = (int) (nuestro.ataques[toChooseAtaque].power+(nuestro.atk*0.2f) - enemigo.def*0.1f < 0 
								? (nuestro.ataques[toChooseAtaque].power+(nuestro.atk*0.2f))*0.1f
										: nuestro.ataques[toChooseAtaque].power+(nuestro.atk*0.2f) - enemigo.def*0.1f);
						
						enemigo.hp = enemigo.hp - damages < 0 ? 0 : enemigo.hp - damages;
						System.out.println(nuestro.name + " ha usado " + nuestro.ataques[toChooseAtaque].name);
						txtEnemigoHp.changeImage(("PS " + enemigo.name + " = " + enemigo.hp));

						break;
					case 2:
						//TODO
					case 3:
						//TODO
					case 4:
						if(r.nextBoolean()) {
							System.out.println("Has huido");
							huirDeCombate = true;
							break;
						}
					default:
						continue;
						
				}
			}else {
				Thread.sleep(1000);
				Ataque selectedAtack = null;
				int maxBucle = 100;
				while(selectedAtack==null && maxBucle > 1) {
					selectedAtack = enemigo.ataques[r.nextInt(enemigo.ataques.length)];
					maxBucle--;
				}
				if(selectedAtack == null) 
					continue;
				
				int damages = (int) (selectedAtack.power+(enemigo.atk*0.2f) - nuestro.def *0.1f < 0 
						? (selectedAtack.power+(enemigo.atk*0.2f)) * 0.1f
								: selectedAtack.power+(enemigo.atk*0.2f) - nuestro.def *0.1f);
				
				nuestro.hp = nuestro.hp - damages < 0 ? 0 : nuestro.hp - damages;
				System.out.println(enemigo.name+" ha usado "+selectedAtack.name);
				Thread.sleep(1000);
				
				txtNuestroHp.changeImage(("PS " + nuestro.name + " = " + nuestro.hp));
				if(huirDeCombate)
					acabarCombate = true;
							
			}
			if (nuestro.hp <= 0) {
				//TODO, Aqu� comprobar si hay otros pokemons que se puedan sacar al combate
				combateGanado = false;
				nuestro.hp = nuestro.maxHp; //REGENERAR VIDA
				acabarCombate = true;
			}else if (enemigo.hp <= 0){
				combateGanado = true;
				acabarCombate = true;
			}else {
				nuestroTurno = !nuestroTurno;
			}
			f.draw(toDraw);
			Thread.sleep(33);

		}
		System.out.println("Fuera combate");
		return combateGanado;
	}

	/**
	 * Pulsamos con el raton , lo haras para atacar al pokemon(esto de momento esta asi, en un futuro se hara mejor)
	 */
	private static int checkMouseInt(ArrayList<Sprite> toDraw) {
		int x = f.getMouseX();
		int y = f.getMouseY();
		for (Sprite s : toDraw) {
			if(s instanceof Boton && x != lastMousePosition[0] && y != lastMousePosition[1] && x >= s.x1 && x <= s.x2 && y >= s.y1 && y <= s.y2) {
				Boton but = (Boton) s;
				System.out.println("He hecho click en "+s.name);
				lastMousePosition[0] = x;
				lastMousePosition[1] = y;
				return Integer.parseInt(but.name);
			}
		}
		return -1;
	}

	/**
	 * Chocamos con la pokeball del mapa que nos da a nuestro pokemon 
	 */
	private static void checkForPokeballs() {
	
		if(w.getPressedKeys().contains('q')) {
			Sprite collision = new Sprite("", 0, 0, 0, 0, "");
			switch (pj.mirandoHacia) {
			case 's':
				collision = new Sprite("coll", pj.x1, pj.y2+1, pj.x2, pj.y2+8, "Pueblo1.png");
				break;
			case 'w':
				collision = new Sprite("coll", pj.x1, pj.y1-8, pj.x2, pj.y1-1, "Pueblo1.png");
				break;
			case 'a':
				collision = new Sprite("coll", pj.x1-9, pj.y1, pj.x1-1, pj.y2, "Pueblo1.png");
				break;
			case 'd':
				collision = new Sprite("coll", pj.x2+1, pj.y1, pj.x2+9, pj.y2, "Pueblo1.png");
				break;
			}
			
			ArrayList<Sprite> colls = collision.collidesWithField(f);
			for (int i = 0; i < colls.size(); i++) {
				if(colls.get(i) instanceof Pickable) {
					Pickable item = (Pickable) colls.get(i);
					item.pick();
					sprites.remove(colls.get(i));
					break;
				}			
			}
			
		}else if(w.getPressedKeys().contains('e')) {
			for (int i = 0; i < pj.actualPokemons; i++) {
				System.out.println(i+" "+pj.pokemonInventory[i]);
			}
		}
	}

	private static void checkForEntrenadores() throws InterruptedException {
		Sprite collision = null;
		Sprite collision1 = null;
		Sprite collision2 = null;
		Entrenador ent = null;
		EntrenadorC ent1 = null;
		EntrenadorE ent2 = null;
		for (int i = 0; i < sprites.size(); i++) {
			if(sprites.get(i) instanceof Entrenador) {
				ent = (Entrenador) sprites.get(i);
				if(!ent.derrotado)
					collision = new Sprite("coll", ent.x1, ent.y1, ent.x2, ent.y2+80, "");
			}
			if(sprites.get(i) instanceof EntrenadorC) {
				ent1 = (EntrenadorC) sprites.get(i);
				if(!ent1.derrotado)
					collision1 = new Sprite("coll", ent1.x1, ent1.y1, ent1.x2, ent1.y2+80, "");
			}
			if(sprites.get(i) instanceof EntrenadorE) {
				ent2 = (EntrenadorE) sprites.get(i);
				if(!ent2.derrotado)
					collision2 = new Sprite("coll", ent2.x1, ent2.y1, ent2.x2, ent2.y2+80, "");
			}
		}
		if(collision != null) {
			
			ArrayList<Sprite> colls = collision.collidesWithField(f);
			if(colls.contains(pj)) {
				for (int i = 0; i < ent.actualPokemons; i++) {
					
					Pokemon nuestro = null;					
					if(pj.actualPokemons > 0) {
						for (int j = 0; j < pj.actualPokemons; j++) {
							if(pj.pokemonInventory[j].hp > 0) {
								nuestro = pj.pokemonInventory[j];
								break;
							}
						}
						Pokemon pokemonAPelear =  new Pokemon(ent.pokemonInventory[i]);
						ent.derrotado = Icombate(nuestro, new Pokemon(ent.pokemonInventory[i]), true);
						System.out.println("COMBATE ACABADO: pokemon num"+ i+" HP:"+ pokemonAPelear.hp);
						if(!ent.derrotado) {
							pj.moverPj(-(ent.x2-ent.x1), 0);
							break;
						}
					}
				}
			}
		}
		
		if(collision1 != null) {
			ArrayList<Sprite> colls = collision1.collidesWithField(f);
			if(colls.contains(pj)) {
			for (int i = 0; i < ent1.actualPokemons; i++) {
				
				Pokemon nuestro = null;					
				if(pj.actualPokemons > 0) {
					for (int j = 0; j < pj.actualPokemons; j++) {
						if(pj.pokemonInventory[j].hp > 0) {
							nuestro = pj.pokemonInventory[j];
							break;
						}
					}
					Pokemon pokemonAPelear =  new Pokemon(ent1.pokemonInventory[i]);
					ent1.derrotado = Icombate(nuestro, new Pokemon(ent1.pokemonInventory[i]), true);
					System.out.println("COMBATE ACABADO: pokemon num"+ i+" HP:"+ pokemonAPelear.hp);
					if(!ent1.derrotado) {
						pj.moverPj(-(ent1.x2-ent1.x1), 0);
						break;
					}
				}
			}
		}
	}
		
		if(collision2 != null) {
			ArrayList<Sprite> colls = collision2.collidesWithField(f);
			if(colls.contains(pj)) {
			for (int i = 0; i < ent2.actualPokemons; i++) {
				
				Pokemon nuestro = null;					
				if(pj.actualPokemons > 0) {
					for (int j = 0; j < pj.actualPokemons; j++) {
						if(pj.pokemonInventory[j].hp > 0) {
							nuestro = pj.pokemonInventory[j];
							break;
						}
					}
					Pokemon pokemonAPelear =  new Pokemon(ent2.pokemonInventory[i]);
					ent2.derrotado = Icombate(nuestro, new Pokemon(ent2.pokemonInventory[i]), true);
					System.out.println("COMBATE ACABADO: pokemon num"+ i+" HP:"+ pokemonAPelear.hp);
					if(!ent2.derrotado) {
						pj.moverPj(-(ent2.x2-ent2.x1), 0);
						break;
					}
				}
			}
		}
	}
}


	/**
	 * Inicio, aqui tenemos varias listas de sprites, posiciones., etc
	 */
	private static void init() {
		w.setSize(550, 400);
		//w.setResizable(false);
		f.resize();

		ListaPokemon.myPokemons();
		ListaPokemon.otherPokemons();
		
		
		et1 = new Entrenador("volcan", 510, 156, 531, 180, "res/Volcan.png");
		et1.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.Magikarp));
		et1.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.Magikarp));
		et2 = new EntrenadorC("cintia",150,372,171,396, "res/cintia.png");
		et2.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.Dragonite));
		et2.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.Zeraora));
		et3 = new EntrenadorE("electro",470,608,491,632, "res/electro.png");
		et3.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.Magnezone));
		et3.AddPokemonToInventory(ListaPokemon.pokemons.get(PokemonName.PikachuAsh));
		
		pokeluca = new Pokeluca(382, 324, 408, 353, PokemonName.Lucario, "res/MasterLucario.png");
		System.out.println(pokeluca.pokemon.name);
		
		
		sprites.add(mp);
		sprites.add(pj);
		sprites.add(casaAmarilla);
		sprites.add(hierba);
		sprites.add(pokeluca);
		sprites.add(et1);
		sprites.add(et2);
		sprites.add(et3);
		
		
	}
	
	

}

