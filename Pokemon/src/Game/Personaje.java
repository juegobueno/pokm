package Game;

import java.util.HashSet;


import Core.Sprite;
import Mounstruitos.Ataque;
import Mounstruitos.Pokemon;

/**
 * Todos los detalles del pj
 * @author Ismael
 *
 */

public class Personaje extends Sprite {
	
	/**
	 * @param speed - velocidad al moverse
	 * @param mirandoHacia - direcion en la que miramos
	 * @param maxPokemons - cantidad max que podemos llevar
	 * @param actualPokemons - el pokemon actual que tenemos o los que tenemos
	 */
	private static Personaje instance = null;
	
	int speed = 4;
	public char mirandoHacia = 's';
	private int[][] tchar; // Lista de textos al hablar

	private int maxPokemons = 6;
	public int actualPokemons = 0;
	public Pokemon[] pokemonInventory = new Pokemon[maxPokemons];


	
	
	/**
	 * Nosotros
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param path - ponemos nuestro pj que queremos que ser
	 * @param att - los ataques que tiene nuestro pokemon,esto se hara proximante...
	 * @param pokemonInventoryOculto - el que tenemos en nuestro inventario
	 */
	private Personaje() {
		super("Prota", 274, 192, 295, 216, "res/quinoa.png");
		Ataque[] att = {};
		for (int i = 0; i < pokemonInventory.length; i++) {
			pokemonInventory[i] = new Pokemon(0, 0, 0, 0, 0, 0, "None", att, "");
		}
	}
	
	public static Personaje getPersonaje() {
		if(instance == null) {
			instance = new Personaje();
		}
		return instance;
	}
	
	/**
	 * Movimiento pj 
	 * Controles------- a,s,d,w      -----  p para ir mas rapido -------  q para obtener la pokeball 
	 * ------- e para ver el pokemon del inventario
	 */
	public void pjInput() {
		//System.out.println(x1+" "+y1+" : "+x2+" "+y2);
		if(Juego.w.getPressedKeys().contains('a')) {
			x1 -= speed;
			x2 -= speed;
			Juego.pj.changeImage("res/izqstatic.png");
			if(x1>400) {
				Juego.f.lockScroll(this, Juego.w);
			}else {
				Juego.f.lockScroll=false;
				//Juego.f.scroll(speed, 0);
				
			}
			mirandoHacia = 'a';
				
		}
		if(Juego.w.getPressedKeys().contains('d')) {
			x1 += speed;
			x2 += speed;
			Juego.pj.changeImage("res/der.png");
			if(x1>400) {
				Juego.f.lockScroll(this, Juego.w);
			}else {
				Juego.f.lockScroll=false;
				//Juego.f.scroll(speed, 0);
				
			}
			mirandoHacia = 'd';
		}
		if(Juego.w.getPressedKeys().contains('w')) {
			y1 -= speed;
			y2 -= speed;
			Juego.pj.changeImage("res/arriba.png");
			Juego.f.scroll(0, speed);
			mirandoHacia = 'w';
		}
		if(Juego.w.getPressedKeys().contains('s')) {
			y1 += speed;
			y2 += speed;
			Juego.pj.changeImage("res/quinoa.png");
			Juego.f.scroll(0, -speed);
			mirandoHacia = 's';
		}
		if(Juego.w.getPressedKeys().contains('p')) {
			speed = 6;
		}else {
			speed = 4;
		}
		
	}
	
	public void moverPj(int x, int y) {
		this.x1 += x;
		this.x2 += x;
		this.y1 += y;
		this.y2 += y;
	}

	/**
	 * A�adir pokemon al inventario
	 */
	public void AddPokemonToInventory(Pokemon pokemon) {
		if(actualPokemons<maxPokemons) {
			this.pokemonInventory[actualPokemons] = pokemon;
			actualPokemons++;
		}
	}
	
	

}

