package Game;

import Core.Sprite;
/**
* Contiene la hierba del combate
* @author Ismael
*
*/

public class Hierba extends Sprite  {
	
	/**
	 * Hierba de la colision
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param path - se deja sin nada
	 */


	public Hierba(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}

}

