package Game;

import Core.Sprite;
import Mounstruitos.Pokemon;

public class Entrenador extends Sprite{

	public Pokemon[] pokemonInventory = new Pokemon[6];
	public int actualPokemons = 0;
	public boolean derrotado = false;
	
	public Entrenador(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		derrotado = false;
	}
	
	/**
	 * A�adir pokemon al inventario
	 */
	public void AddPokemonToInventory(Pokemon p) {
		Pokemon toAdd = new Pokemon(p);
		if(actualPokemons<6) {
			this.pokemonInventory[actualPokemons] = toAdd;
			actualPokemons++;
		}
	}


}
