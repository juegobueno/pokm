package Game;

import Core.Sprite;
import Mounstruitos.ListaPokemon;
import Mounstruitos.Pokemon;
import Mounstruitos.PokemonName;


/**
 * Pokeball de lucario
 * @author Ismael
 *
 */
public class Pokeluca extends Sprite implements Pickable {
	 /*
	  * Aqui tendremos todas las estadisticas de los pokemon
	  */
	
	public Pokemon pokemon;
	
	/**
	 * Colision pared 
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param pokemon - estadisticas poke
	 * @param path - se deja sin nada
	 */

	
	public Pokeluca(int x1, int y1, int x2, int y2, PokemonName pokemon, String path) {
		super("", x1, y1, x2, y2, path);
		this.physicBody = true;
		this.solid = true;
		this.terrain = true;
		this.pokemon = ListaPokemon.pokemons.get(pokemon);
		this.pokemon.name = pokemon+"";
		this.MARGIN = 0;
	}
	
	
	/**
	 * Colision pared 
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param path - se deja sin nada
	 */

	public Pokeluca(int x1, int y1, int x2, int y2, String path) {
		super("", x1, y1, x2, y2, path);
		this.terrain = true;
		this.solid = true;
		this.physicBody = true;
		this.MARGIN = 0;
	}

	/*
	 * aqui cambiaremos de pokemon por uno de la lista,( se a�adiran mas segun se vaya avanzando)
	 */
	public void ChangePokemon(PokemonName pokemon) {
		this.pokemon = ListaPokemon.pokemons.get(pokemon);
	}


	@Override
	public void pick() {
		Personaje.getPersonaje().AddPokemonToInventory(this.pokemon);
		System.out.println("Estas obteniendo"+pokemon+ "de la pokeball " );
		
	}


	
}
