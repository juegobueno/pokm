package Game;

import Core.Sprite;

/**
* Contiene la col con la pared
* @author Ismael
*
*/
public class ColPared extends Sprite {
	
	/**
	 * Colision pared 
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param path - se deja sin nada
	 */


	public ColPared(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.terrain = true;
		this.MARGIN = 0;
	}

}
