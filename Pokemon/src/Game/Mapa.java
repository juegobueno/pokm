package Game;

import Core.Sprite;

/**
 * Mapa por el que se mueve nuestro pj
 * @author Ismael
 *
 */

public class Mapa extends Sprite {
	
	/**
	 * Mapa del juego
	 * @param name - nombre
	 * @param x1- Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param x2 - Posicion inferior izquierda horiz
	 * @param y2 - Posicion inferior izquierda verti
	 * @param path - aqui pegamos nuestro mapa 
	 */
	
	public Mapa(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub

	}


}
