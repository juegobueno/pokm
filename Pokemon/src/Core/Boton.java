package Core;

import java.awt.Font;

import Core.Sprite;


/**
 * Genera un boton
 * contiene los botones que usamos en los combates pokemon
 * @see Game.Juego
 * @author Ismael
 */

public class Boton extends Sprite{
	
	
	/**
	 * Auto-Enabled button
	 * @param name - Inner engine name
	 * @param x1 - Top left position horizontally
	 * @param y1 - Top left position vertically
	 * @param x2 - Bottom right position horizontally
	 * @param y2 - Bottom right position vertically
	 * @param path - The source of the sprite
	 */

	public Boton(String name, int x1, int y1, int x2, int y2) {
		super(name, x1, y1, x2, y2, "");
		this.unscrollable = true;
	}
	public Boton(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.unscrollable = true;
	}

}
