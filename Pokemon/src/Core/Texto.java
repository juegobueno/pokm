package Core;

import java.awt.Font;
import Core.Sprite;

/**
 * Clase simple que contiene textos
 * @author Ismael
 *
 */
public class Texto extends Sprite{
	/**
	 * Creacion textos
	 * @param Font - fuente letra
	 * @param int - color letra
	 */


	public static Font normalFont = new Font("Monospaced", Font.BOLD, 50);
	public static Font boldFont = new Font("Monospaced", Font.BOLD, 100);
	public static Font lightFont = new Font("Monospaced", Font.BOLD, 25);
	public static int whiteColor = 0xFFFFFF;
	public static int blackColor = 0x000000;
	
	/**
	 * Creacion textos
	 * @param name - Nombre interno
	 * @param text - texto a mostrar
	 * @param x1 - Posicion superior izquierda horiz
	 * @param y1 - Posicion superior izquierda verti
	 * @param path - peque�o dibujo de board
	 */


	public Texto(String name, int x1, int y1, String path) {
		super(name, x1, y1, x1, y1, path);
		this.text=true;
		this.unscrollable = true;
	}

}
