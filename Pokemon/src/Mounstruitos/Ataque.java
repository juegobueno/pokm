package Mounstruitos;

import Movimientos.Tipos;

/**
 * poder de los ataques
 * @author Ismael
 *
 */

public class Ataque {
	/**
	 * @param name - nombre ataque
	 * @param power - poder del ataque
	 * @param categoria - fisico o especial
	 */

	
	public String name;
	public int power;
	public Tipos categoria;
	
	/*
	 * se hace el constructor de los public
	 */
	
	public Ataque(String name, int power, Tipos categoria) {
		super();
		this.name = name;
		this.power = power;
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return "Ataque [name=" + name + ", power=" + power + ", categoria=" + categoria + "]";
	}
	
	

}
