package Mounstruitos;

import java.util.Arrays;


import Core.Sprite;


/**
* Estadisticas de los poke
* @author Ismael
*
*/

public class Pokemon extends Sprite{
	
	/**
	 * Abreviaturas de los distintos tipos de estadisticas de los pokemon
	 * @param hp- vida poke
	 * @param atk - ataque poke
	 * @param def - defensa poke
	 * @param satk - ataque especial poke
	 * @param sdef - defensa espcial poke
	 * @param spd - velocidad poke
	 * @param nom - nombre de nuestro poke
	 * @param sprite - imagen de el
	 * @param ataques - fisico o especial
	 */

	public int hp = 0;
	public int maxHp = 0;
	public int atk = 0;
	public int def = 0;
	public int satk = 0;
	public int sdef = 0;
	public int spd = 0;
	public int sprite;
	public Ataque[] ataques = new Ataque[4];
	
	
	/* 
	 * Constructor de lo que pusimos arriba 
	 */
	public Pokemon(int maxHp, int atk, int def, int satk, int sdef, int spd, String name, Ataque[] ataques, String sprite) {
		super(name, 0, 0, 0, 0, sprite);
		this.maxHp = maxHp;
		this.atk = atk;
		this.def = def;
		this.satk = satk;
		this.sdef = sdef;
		this.spd = spd;
		this.ataques = ataques;
		this.hp = maxHp;
	}
	
	public Pokemon(Pokemon p) {
		super(p.name, 0, 0, 0, 0, p.path);
		this.maxHp = p.maxHp;
		this.atk = p.atk;
		this.def = p.def;
		this.satk = p.satk;
		this.sdef = p.sdef;
		this.spd = p.spd;
		this.ataques = p.ataques;
		this.hp = p.maxHp;
	}
	public void addAtaque(Ataque ataque) {
		if(ataques[3] != null) {
			System.out.println("Ya tiene todos los ataques posibles, no puedes a�adir otro");
		}else {
			for (int i = ataques.length-1; i >= 0; i--) {
				if(ataques[i] == null) {
					ataques[i] = ataque;
					break;
				}
			}
		}
	}

	@Override
	public String toString() {
		return "Pokemon [name=" + name + "]";
	}
	
	
	
}
