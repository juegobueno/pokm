package Mounstruitos;

import java.util.HashMap;


import Movimientos.CategoriaMov;
import Movimientos.Tipos;


/**
 * Contiene los pokemon 
 * @author Ismael
 *
 */
public class ListaPokemon {
	

/**
 * contiene la lista de pokemon y sus estadisticas
 * en un futuro se a�aden los movimientos
 */

	public static HashMap<PokemonName, Pokemon> pokemons = new HashMap<PokemonName, Pokemon>();
	/*private static Movimiento esferaaural = new Movimiento(Tipos.LUCHA,CategoriaMov.FISICO,80,98,12,"Esfera Aural","");
	private static Movimiento pu�ohielo = new Movimiento(Tipos.HIELO,CategoriaMov.FISICO,70,80,15,"Pu�o Hielo","");
	private static Movimiento danzaespada = new Movimiento(Tipos.NORMAL,CategoriaMov.FISICO,0,100,30,"Danza Espada","");
	private static Movimiento bolasombra = new Movimiento(Tipos.FANTASMA,CategoriaMov.ESPECIAL,85,100,16,"Bola Sombra","");
	*/
	
	

/**
 * A�adimos los pokemons y sus ataques
 * En este caso solo tenemos 2 pokemon
 */
	public static void myPokemons() {
		Ataque[] ataquesLucario = {new Ataque("Mordisco", 50, Tipos.TIERRA), new Ataque("Esfera aural", 999, Tipos.HADA),
				new Ataque("Escupitajo", 1, Tipos.AGUA), new Ataque("Cabezazo",80, Tipos.HIELO )};
		Pokemon Lucario = new Pokemon(130, 150, 70, 115, 70, 90, "", ataquesLucario, "res/Lucario_espalda_G5_3.gif");
		pokemons.put(PokemonName.Lucario, Lucario);
	}
	
	public static void otherPokemons() {
		Ataque[] ataquesMagikarp = {new Ataque("Salpicadura", 50, Tipos.TIERRA),new Ataque("Chapuzon", 70, Tipos.FANTASMA),new Ataque("Navajazo", 30, Tipos.BICHO),
				new Ataque("Tortazo", 40, Tipos.NORMAL)};
		Pokemon Magikarp = new Pokemon(100, 120, 81, 122, 14, 54, PokemonName.Magikarp+"", ataquesMagikarp, "res/Magikarp.gif");
		pokemons.put(PokemonName.Magikarp, Magikarp);
		
		Ataque[] ataquesMetapod = {new Ataque("Refuerzo", 0, Tipos.NORMAL),new Ataque("RompeCoraza", 60, Tipos.HADA),new Ataque("Carterazo", 100, Tipos.FANTASMA),
				new Ataque("Lechugazo", 80, Tipos.PLANTA)};
		Pokemon Metapod = new Pokemon(50, 20, 55, 25, 25, 30, PokemonName.Metapod+"", ataquesMetapod, "res/Metapod_NB_variocolor.gif");
		pokemons.put(PokemonName.Metapod, Metapod);
		
		Ataque[] ataquesDragonite = {new Ataque("Palo", 50, Tipos.TIERRA),new Ataque("Hiperrayo", 150, Tipos.NORMAL),new Ataque("Rayo Hielo", 78, Tipos.HIELO),
				new Ataque("Garra Dragon", 64, Tipos.DRAGON)};
		Pokemon Dragonite = new Pokemon(91, 134, 95, 100, 100, 80, PokemonName.Dragonite+"", ataquesDragonite, "res/Dragonite_NB_variocolor.gif");
		pokemons.put(PokemonName.Dragonite, Dragonite);
		
		Ataque[] ataquesZeraora = {new Ataque("Pu�o Trueno", 80, Tipos.ELECTRICO),new Ataque("Abocajarro", 130, Tipos.LUCHA),new Ataque("Placaje Electrico", 180, Tipos.ELECTRICO),
				new Ataque("Fuerza", 60, Tipos.NORMAL)};
		Pokemon Zeraora = new Pokemon(88, 112, 75, 102, 80, 143, PokemonName.Zeraora+"", ataquesZeraora, "res/Zeraora_USUL_variocolor.gif");
		pokemons.put(PokemonName.Zeraora, Zeraora);
		
		Ataque[] ataquesPikachuAsh = {new Ataque("Rayo", 60, Tipos.ELECTRICO),new Ataque("Tela Electrica", 10, Tipos.BICHO),new Ataque("Terremoto", 100, Tipos.FANTASMA),
				new Ataque("Vuelo", 80, Tipos.NORMAL)};
		Pokemon PikachuAsh = new Pokemon(90, 100, 70, 92, 67, 120, PokemonName.PikachuAsh+"", ataquesPikachuAsh, "res/pikachu-kantocap.gif");
		pokemons.put(PokemonName.PikachuAsh, PikachuAsh);
		
		Ataque[] ataquesMagnezone = {new Ataque("Pantalla", 20, Tipos.HADA),new Ataque("Carglass", 100, Tipos.HIELO),new Ataque("Robo Cartera", 40, Tipos.PLANTA),
				new Ataque("Planchazo", 50, Tipos.NORMAL)};
		Pokemon Magnezone = new Pokemon(124, 70, 99, 130, 80, 100, PokemonName.Magnezone+"", ataquesMagnezone, "res/Magnezone_XY_variocolor.gif");
		pokemons.put(PokemonName.Magnezone, Magnezone);
		
		Ataque[] ataquesFerrothorn = {new Ataque("Puas toxicas", 50, Tipos.VENENO),new Ataque("Fortaleza", 10, Tipos.HIELO),new Ataque("Tentaculo triple", 100, Tipos.PLANTA),
				new Ataque("Robo de nomina", 50, Tipos.NORMAL)};
		Pokemon Ferrothorn = new Pokemon(140, 80, 50, 140, 100, 150, PokemonName.Ferrothorn+"", ataquesFerrothorn, "res/Ferrothorn_XY.gif");
		pokemons.put(PokemonName.Ferrothorn, Ferrothorn);
		
		Ataque[] ataquesRubenWopper = {new Ataque("Puas toxicas", 50, Tipos.VENENO),new Ataque("Fortaleza", 10, Tipos.HIELO),new Ataque("Tentaculo triple", 100, Tipos.PLANTA),
				new Ataque("Robo de nomina", 50, Tipos.NORMAL)};
		Pokemon RubenWopper = new Pokemon(140, 95, 50, 140, 100, 150, PokemonName.RubenWopper+"", ataquesRubenWopper, "res/RubenWopper.gif");
		pokemons.put(PokemonName.RubenWopper, RubenWopper);
		

	}
	
	

}